<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barrio extends Model
{
    protected $table = "barrios";

    protected $fillable = [
        'barrios',
    ];

    public function trabajadores()
    {
        return $this->hasMany('App\Trabajador');
    }

    public function contratantes()
    {
        return $this->hasMany('App\Contratante');
    }
}
