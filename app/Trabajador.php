<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trabajador extends Model
{
    protected $table = "trabajadores";

    protected $fillable = [
        'nombres', 'direcion', 'doc_ide', 'fech_naci', 'lugar_naci', 'ciudad_id', 'barrio_id', 'telefono_id', 'user_id',
    ];

    public function barrio()
    {
        return $this->belongsTo('App\Barrio');
    }

    public function ciudad()
    {
        return $this->belongsTo('App\Ciudad');
    }

    public function telefono()
    {
        return $this->belongsTo('App\Telefono');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function records()
    {
        return $this->hasMany('App\RecordTrab');
    }

    public function imagenes()
    {
        return $this->hasMany('App\imagen_trab');
    }

    public function edad($fecha)
    {
        list($a, $m, $d) = explode("-", $fecha);

        $dia = date('j');
        $mes = date('n');
        $anio = date('Y');

        if ($mes == $m && $dia > $d) {
            $anio = $anio-1;
        }
        if ($mes < $m) {
            $anio = $anio-1;
        }

        $edad = $anio-$a;

        return $edad;
    }

    public function scopeBuscarT($query, $busqueda)
    {
        return $query->where('doc_ide', 'LIKE', "%$busqueda%");
    }
}
