<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contratante extends Model
{
    protected $table = "contratantes";

    protected $fillable = [
        'nombres', 'direcion', 'doc_ide', 'pers_atend', 'adultos', 'terc_edad', 'ninos', 'pisos', 'mascotas', 'imagen', 'Salario', 'diasdescanso', 'ttrabajo', 'ciudad_id', 'barrio_id', 'telefono_id', 'predio_id', 'user_id',
    ];

    public function barrio()
    {
        return $this->belongsTo('App\Barrio');
    }

    public function ciudad()
    {
        return $this->belongsTo('App\Ciudad');
    }

    public function telefono()
    {
        return $this->belongsTo('App\Telefono');
    }

    public function predio()
    {
        return $this->belongsTo('App\TipoPredio');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function records()
    {
        return $this->hasMany('App\RecordContr');
    }

    public function scopeBuscarC($query, $busqueda)
    {
        return $query->where('doc_ide', 'LIKE', "%$busqueda%");
    }
}
