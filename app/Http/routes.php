<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('pdf', 'ControladorPdf@invoice');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/home', [
        'uses'  => 'HomeController@index',
        'as'    => 'home'
    ]);

    Route::group(['middleware' =>  'admins'], function (){

        Route::get('/buscar', [
            'uses' => 'HomeController@resulBusqueda',
            'as' => 'buscar',
        ]);

        Route::post('/buscar', [
            'uses' => 'HomeController@resulBusqueda',
            'as' => 'buscar',
        ]);

        Route::resource('users', 'ControladorUsers');

        Route::get('/eliminarcontr/{contratante}', [
            'uses' => 'ControladorContratantes@destroy',
            'as' => 'contratantes.eliminar',
        ]);

        Route::get('/eliminartrab/{trabajador}', [
            'uses' => 'ControladorTrabajadores@destroy',
            'as' => 'trabajadores.eliminar',
        ]);

        Route::get('/trabajadores/{trabajador}/edit', [
            'uses' => 'ControladorTrabajadores@edit',
            'as' => 'trabajadores.edit',
        ]);

        Route::get('/contratantes/{contratante}/edit', [
            'uses' => 'ControladorContratantes@edit',
            'as' => 'contratantes.edit',
        ]);

        Route::get('/eliminaruser/{usuario}', [ 
            'uses' => 'ControladorUsers@destroy',
            'as' => 'users.destroy',
        ]);
    });

    Route::group(['middleware' => 'gestor'], function (){

        Route::resource('contratantes', 'ControladorContratantes');

        Route::resource('trabajadores', 'ControladorTrabajadores');

    });

    Route::get('/ver_usuario/{usuario}', [
        'uses' => 'ControladorUsers@show',
        'as' => 'users.show',
    ]);

    Route::get('/pdfcontratante/{contratante}', [
        'uses'  => 'ControladorPdf@pdfContratante',
        'as'    => 'contratantes.pdf'
    ]);

    Route::get('/pdftrabajador/{trabajador}', [
        'uses'  => 'ControladorPdf@pdfTrabajador',
        'as'    => 'trabajadores.pdf'
    ]);
    
});

Route::get('/', [
    'uses'  => 'Auth\AuthController@getLogin',
    'as'    => 'auth.login'
]);
Route::post('/', [
    'uses'  => 'Auth\AuthController@postLogin',
    'as'    => 'auth.login'
]);
Route::get('/logout', [
    'uses'  => 'Auth\AuthController@getLogout',
    'as'    => 'auth.logout'
]);

Route::get('/recupcontraseña', [
    'uses'  => 'ControladorClave@recuperarform',
    'as'    => 'claves.restore'
]);

Route::post('/recupcontraseña', [
    'uses'  => 'ControladorClave@verificar',
    'as'    => 'claves.verificar'
]);

Route::put('/actcontraseña/{usuario}', [
    'uses'  => 'ControladorClave@actualizar',
    'as'    => 'claves.actualizar'
]);