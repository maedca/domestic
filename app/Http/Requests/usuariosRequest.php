<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class usuariosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'usuario'   => 'min:4|alpha_dash|required|unique:users,user',
            'tusuario'  => 'required',
            'correo'    => 'email|required|unique:users,correo',
            'clave'     => 'alpha_num|min:4|required',
            'rep_clave'    => 'same:clave|required',
        ];
    }
}
