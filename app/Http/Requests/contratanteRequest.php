<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class contratanteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombres' => 'min:4|max:255|required',
            'direccion' => 'required',
            'ciudad' => 'required',
            'barrio' => 'required',
            'documento' => 'max:10|required|unique:contratantes,doc_ide',
            'tpredio' => 'required',
            'personas' => 'numeric|required',
            'adultos' => 'numeric|required',
            'pisos' => 'numeric|required',
            'mascotas' => 'numeric|required',
            'tedad' => 'numeric|required',
            'ninos' => 'numeric|required',
            'salario' => 'required',
            'ddescanso' => 'required',
            'ttrabajo' => 'required',
            'gestion' => 'required',
            'imagen' => 'mimes:jpg,jpeg,png,gif',
            'telefono' => 'max:10',
            'celular' => 'max:10'
        ];
    }
}
