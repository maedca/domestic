<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class trabajadorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombres' => 'min:4|max:255|required',
            'direccion' => 'required',
            'ciudad' => 'required',
            'barrio' => 'required',
            'documento' => 'required|unique:trabajadores,doc_ide',
            'fechnac' => 'date|required',
            'lugar' => 'required',
            'gestion' => 'required',
            'imagenes' => 'required'
        ];
    }
}
