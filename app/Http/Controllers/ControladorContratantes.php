<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\contratanteRequest;
use App\Http\Controllers\Controller;
use App\Contratante;
use App\Ciudad;
use App\Barrio;
use App\TipoPredio;
use App\Telefono;
use App\User;
use App\RecordContr;
use Carbon\Carbon;
use Laracasts\Flash\Flash;

class ControladorContratantes extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contratantes = Contratante::orderBy('created_at', 'ASC')->get();

        return view('listcontratante')->with('contratantes', $contratantes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ciudades = Ciudad::orderBy('ciudad', 'ASC')->lists('ciudad', 'id');

        return view('crearcontratante')->with('ciudades', $ciudades);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(contratanteRequest $request)
    {
        //Guardando la imagen
        if($request->file('imagen')){
            $file = $request->file('imagen');
            $nombre = $request->documento.'.'.$file->getClientOriginalExtension();
            $ruta = public_path().'/img/img_contr/';
            $file->move($ruta, $nombre);
        }

        $barrios = Barrio::where('barrios', "$request->barrio")->get();
        $tpredios = TipoPredio::where('predio', "$request->tpredio")->get();
        
        if (count($barrios) == 0) {
            $barrio = new Barrio();
            $barrio->barrios = $request->barrio;
            $barrio->save();
        } else {
            foreach ($barrios as $barrio) {
                $barrio = $barrio;
            }
        }

        if (count($tpredios) == 0) {
            $tpredio = new TipoPredio();
            $tpredio->predio = $request->tpredio;
            $tpredio->save();
        } else {
            foreach ($tpredios as $tpredio) {
                $tpredios = $tpredio;
            }
        }

        if ($request->telefono != null || $request->celular != null) {
            $telefonos = new Telefono();

            $telefonos->telefono = $request->telefono;
            $telefonos->celular = $request->celular;
            $telefonos->save();
        }

        $contratante = new Contratante();

        $contratante->nombres = $request->nombres;
        $contratante->direccion = $request->direccion;
        $contratante->doc_ide = $request->documento;
        $contratante->pers_atend = $request->personas;
        $contratante->adultos = $request->adultos;
        $contratante->terc_edad = $request->tedad;
        $contratante->ninos = $request->ninos;
        $contratante->pisos = $request->pisos;
        $contratante->mascotas = $request->mascotas;
        if ($request->file('imagen')) {
            $contratante->imagen = '/img/img_contr/'.$nombre;
        }
        $contratante->salario = '$ '.$request->salario;
        $contratante->diasdescanso = $request->ddescanso;
        $contratante->ttrabajo = $request->ttrabajo;
        $contratante->ciudad_id = $request->ciudad;
        $contratante->barrio()->associate($barrio);
        if ($request->telefono != null || $request->celular != null) {
           $contratante->telefono()->associate($telefonos);
        }
        $contratante->predio()->associate($tpredio);
        $contratante->save();

        $record = new RecordContr();

        $record->record = $request->gestion;
        $record->fecha = $request->fecha;
        $record->contratante()->associate($contratante);
        $record->save();

        Flash::success("Empleador registrado sastifactoriamente");

        return redirect()->route('contratantes.show', $contratante);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contratante = Contratante::find($id);

        return view('vercontratante')->with('contratante', $contratante);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contratante = Contratante::find($id);
        $ciudades = Ciudad::orderBy('ciudad', 'ASC')->lists('ciudad', 'id');

        return view('editcontratante')->with('contratante', $contratante)->with('ciudades', $ciudades);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Guardando la imagen
        if($request->file('imagen')){
            $file = $request->file('imagen');
            $nombre = $request->documento.'.'.$file->getClientOriginalExtension();
            $ruta = public_path().'/img/img_contr/';
            $file->move($ruta, $nombre);
        }

        $barrios = Barrio::where('barrios', "$request->barrio")->get();
        $tpredios = TipoPredio::where('predio', "$request->tpredio")->get();
        
        if (count($barrios) == 0) {
            $barrio = new Barrio();
            $barrio->barrios = $request->barrio;
            $barrio->save();
        } else {
            foreach ($barrios as $barrio) {
                $barrio = $barrio;
            }
        }

        if (count($tpredios) == 0) {
            $tpredio = new TipoPredio();
            $tpredio->predio = $request->tpredio;
            $tpredio->save();
        } else {
            foreach ($tpredios as $tpredio) {
                $tpredios = $tpredio;
            }
        }

        if ($request->telefono != null || $request->celular != null) {
            $telefonos = new Telefono();

            $telefonos->telefono = $request->telefono;
            $telefonos->celular = $request->celular;
            $telefonos->save();
        }

        $contratante = Contratante::find($id);

        $contratante->nombres = $request->nombres;
        $contratante->direccion = $request->direccion;
        $contratante->doc_ide = $request->documento;
        $contratante->pers_atend = $request->personas;
        $contratante->adultos = $request->adultos;
        $contratante->terc_edad = $request->tedad;
        $contratante->ninos = $request->ninos;
        $contratante->pisos = $request->pisos;
        $contratante->mascotas = $request->mascotas;
        if ($request->file('imagen')) {
            $contratante->imagen = 'img/img_contr/'.$nombre;
        }
        $contratante->salario = $request->salario;
        $contratante->diasdescanso = $request->ddescanso;
        $contratante->ttrabajo = $request->ttrabajo;
        $contratante->ciudad_id = $request->ciudad;
        $contratante->barrio()->associate($barrio);
        if ($request->telefono != null || $request->celular != null) {
           $contratante->telefono()->associate($telefonos);
        }
        $contratante->predio()->associate($tpredio);
        $contratante->save();

        if ($request->gestion != null) {
            $record = new RecordContr();

            $record->record = $request->gestion;
            $record->fecha = $request->fecha;
            $record->contratante()->associate($contratante);
            $record->save();
        }

        Flash::success("Empleador editado sastifactoriamente");

        return redirect()->route('contratantes.show', $contratante);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contratante = Contratante::find($id);

        $ruta = public_path().$contratante->imagen;

        unlink($ruta);

        $contratante->delete();

        if($contratante->user_id != null){
            $usuario = $contratante->user;

            $usuario = User::find($usuario->id);
            $usuario->delete();
        }

        if($contratante->telefono_id != null){
            $telefono = $contratante->telefono;

            $telefono = Telefono::find($telefono->id);
            $telefono->delete();
        }

        Flash::success("El contratante ha sido eliminado sastifactoriamente");

        return redirect()->route('home');
    }


}
