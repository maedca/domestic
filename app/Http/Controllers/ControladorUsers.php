<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\usuariosRequest;
use App\Http\Controllers\Controller;
use App\Trabajador;
use App\Contratante;
use App\User;
use Laracasts\Flash\Flash;

class ControladorUsers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usuarios = User::orderBy('id', 'ASC')->get();

        return view('crearusuarios')->with('usuarios', $usuarios);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(usuariosRequest $request)
    {
            
        $usuario = new User();

        $usuario->user = $request->usuario;
        $usuario->password = bcrypt($request->clave);
        $usuario->correo = $request->correo;
        $usuario->tipo = $request->tusuario;
        $usuario->save();

        Flash::success('Usuario: '.$usuario->user.' creado sastifactoriamente.');
        return redirect()->route('users.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->clave != $request->clave2){
            Flash::error('Las contraseñas deben ser iguales, edicion no realizada');
            return redirect()->route('users.create');
        }else{
            $usuario = User::find($id);

            $usuario->user = $request->usuario;
            $usuario->tipo = $request->tusuario;
            $usuario->correo = $request->correo;

            if ($request->clave != null) {
                $usuario->password = bcrypt($request->clave);
            }

            $usuario->save();

            Flash::success('El usuario '.$usuario->user.' fue editado sastifactoriamente');
            return redirect()->route('users.create');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::find($id);

        $usuario->delete();

        Flash::success('El usuario '.$usuario->user.' fue eliminado sastifactoriamente');

        return redirect()->route('users.create');
    }
}
