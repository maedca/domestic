<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Trabajador;
use App\Contratante;

class HomeController extends Controller
{
    public function index()
    {
    	return view('home');
    }

    public function resulBusqueda(Request $busqueda)
    {
    	$trabajadores = Trabajador::BuscarT($busqueda->busqueda)->get();
    	$contratantes = Contratante::BuscarC($busqueda->busqueda)->get();

    	return view('resulbuscar')->with('trabajadores', $trabajadores)->with('contratantes', $contratantes);	
    }

}
