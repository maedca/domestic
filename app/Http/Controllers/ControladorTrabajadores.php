<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\trabajadorRequest;
use App\Http\Controllers\Controller;
use App\Trabajador;
use App\Ciudad;
use App\Barrio;
use App\Telefono;
use App\User;
use App\RecordTrab;
use App\imagen_trab;
use Carbon\Carbon;
use Laracasts\Flash\Flash;

class ControladorTrabajadores extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trabajadores = Trabajador::orderBy('created_at', 'ASC')->get();

        return view('listtrabajador')->with('trabajadores', $trabajadores);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ciudades = Ciudad::orderBy('ciudad', 'ASC')->lists('ciudad', 'id');

        return view('crearhojavida')->with('ciudades', $ciudades);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(trabajadorRequest $request)
    {
        $cont = 0;

        foreach ($request->file('imagenes') as $imagen) {
            if ($imagen != null){
                $cont ++;
            }   
        }   
        if ($cont > 0) {

            $cont = 0;
            $rutas = [];

            foreach ($request->file('imagenes') as $imagen) {
                if($imagen != null){
                    $nombre = $request->documento.'_'.$cont.'.'.$imagen->getClientOriginalExtension();
                    $ruta = public_path().'/img/img_trab/';
                    $imagen->move($ruta, $nombre);
                    $rutas += [$cont => '/img/img_trab/'.$nombre];
                    $cont ++;
                }
            }
            $cont = 0;
            $barrios = Barrio::where('barrios', "$request->barrio")->get();
            
            if (count($barrios) == 0) {
                $barrio = new Barrio();
                $barrio->barrios = $request->barrio;
                $barrio->save();
            } else {
                foreach ($barrios as $barrio) {
                    $barrio = $barrio;
                }
            }

            if ($request->telefono != null || $request->celular != null) {
                $telefonos = new Telefono();

                $telefonos->telefono = $request->telefono;
                $telefonos->celular = $request->celular;
                $telefonos->save();
            }

            $trabajador = new Trabajador();

            $trabajador->nombres = $request->nombres;
            $trabajador->direccion = $request->direccion;
            $trabajador->doc_ide = $request->documento;
            $trabajador->fech_naci = $request->fechnac;
            $trabajador->lugar_naci = $request->lugar;
            $trabajador->ciudad_id = $request->ciudad;
            $trabajador->barrio()->associate($barrio);
            if ($request->telefono != null || $request->celular != null) {
               $trabajador->telefono()->associate($telefonos);
            }
            $trabajador->save();

            while ($cont < count($rutas)) {
                $imagen = new imagen_trab();

                $imagen->ruta = $rutas[$cont];
                $imagen->trabajador()->associate($trabajador);
                $imagen->save();

                $cont ++;
            }

            $record = new RecordTrab();

            $record->record = $request->gestion;
            $record->fecha = $request->fecha;
            $record->trabajador()->associate($trabajador);
            $record->save();

            Flash::success("Trabajador registrado sastifactoriamente");

            return redirect()->route('trabajadores.show', $trabajador);
        } else {
            Flash::warning("Debe subir al menos una (1) Imagen");

            return redirect()->route('trabajadores.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trabajador = Trabajador::find($id);

        return view('vertrabajador')->with('trabajador', $trabajador);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trabajador = Trabajador::find($id);
        $ciudades = Ciudad::orderBy('ciudad', 'ASC')->lists('ciudad', 'id');

        return view('edittrabajador')->with('trabajador', $trabajador)->with('ciudades',$ciudades);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Guardando la imagen
        

        $barrios = Barrio::where('barrios', "$request->barrio")->get();
        
        if (count($barrios) == 0) {
            $barrio = new Barrio();
            $barrio->barrios = $request->barrio;
            $barrio->save();
        } else {
            foreach ($barrios as $barrio) {
                $barrio = $barrio;
            }
        }

        if ($request->telefono != null || $request->celular != null) {
            $telefonos = new Telefono();

            $telefonos->telefono = $request->telefono;
            $telefonos->celular = $request->celular;
            $telefonos->save();
        }

        $trabajador = Trabajador::find($id);

        $trabajador->nombres = $request->nombres;
        $trabajador->direccion = $request->direccion;
        $trabajador->doc_ide = $request->documento;
        $trabajador->fech_naci = $request->fechnac;
        $trabajador->lugar_naci = $request->lugar;
        $trabajador->ciudad_id = $request->ciudad;
        $trabajador->barrio()->associate($barrio);
        if ($request->telefono != null || $request->celular != null) {
           $trabajador->telefono()->associate($telefonos);
        }
        $trabajador->save();

        $cont = 0;

        foreach ($request->file('imagenes') as $imagen) {
            if ($imagen != null){
                $cont ++;
            }
        }

        $imagenestrab = imagen_trab::where('trabajador_id', "$id")->get();

        if ($cont > 0) {
            $cont = 0;
            $rutas = [];

            foreach ($request->file('imagenes') as $imagen) {
                if($imagen != null){
                    $nombre = $request->documento.'_'.$cont.'.'.$imagen->getClientOriginalExtension();
                    $ruta = public_path().'/img/img_trab/';
                    $imagen->move($ruta, $nombre);
                    $rutas += [$cont => '/img/img_trab/'.$nombre];
                    $cont ++;
                }
            }
            $cont = 0;
            if (count($imagenestrab) == count($rutas)) {
                foreach ($imagenestrab as $imagentrab) {
                    if ($cont < count($rutas)) {
                        $imagentrab->ruta = $rutas[$cont];
                        $imagentrab->save();
                        $cont ++;
                    }
                }
            }
            $cont = 0;
            foreach ($imagenestrab as $imagentrab) {
                if ($cont < count($rutas)) {
                    $imagentrab->ruta = $rutas[$cont];
                    $imagentrab->save();
                    $cont ++;
                } else {
                    break;
                }
            }
        }

        if ($request->nimagenes != null) {
            $cont = 0;
            foreach ($request->file('nimagenes') as $nimagen) {
                $cont ++;
            }
        }  

        if ($cont > 0) {
            $cont = 0;
            $rutas = [];

            foreach ($request->file('nimagenes') as $nimagen) {
                $dif = count($imagenestrab)+$cont;
                if($nimagen != null){
                    $nombre = $request->documento.'_'.$dif.'.'.$nimagen->getClientOriginalExtension();
                    $ruta = public_path().'/img/img_trab/';
                    $nimagen->move($ruta, $nombre);
                    $rutas += [$cont => '/img/img_trab/'.$nombre];
                    $cont ++;
                }
            }
            $cont = 0;
            while ($cont < count($rutas)) {
                $imagen = new imagen_trab();

                $imagen->ruta = $rutas[$cont];
                $imagen->trabajador()->associate($trabajador);
                $imagen->save();

                $cont ++;
            }
        }

        if ($request->gestion != null) {
            $record = new RecordTrab();

            $record->record = $request->gestion;
            $record->fecha = $request->fecha;
            $record->trabajador()->associate($trabajador);
            $record->save();
        }

        Flash::success("Trabajador editado sastifactoriamente");

        return redirect()->route('trabajadores.show', $trabajador);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trabajador = Trabajador::find($id);

            foreach ($trabajador->imagenes as $imagen) {
                $ruta = public_path().$imagen->ruta;

                unlink($ruta);
            }

        $trabajador->delete();

        if($trabajador->user_id != null){
            $usuario = $trabajador->user;

            $usuario = User::find($usuario->id);
            $usuario->delete();
        }

        if($trabajador->telefono_id != null){
            $telefono = $trabajador->telefono;

            $telefono = Telefono::find($telefono->id);
            $telefono->delete();
        }

        Flash::success("El trabajador ha sido eliminado sastifactoriamente");

        return redirect()->route('home');
    }

}
