<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\verificarUser;
use App\Http\Requests\actualizarUser;
use App\Http\Controllers\Controller;
use App\User;
use Laracasts\Flash\Flash;

class ControladorClave extends Controller
{
    public function recuperarform()
    {
    	return view('claves.recuperar');
    }

    public function verificar(verificarUser $request)
    {
    	$usuarios = User::where('user', "$request->usuario")->where('correo', "$request->correo")->get();

    	if (count($usuarios) > 0) {
    		foreach ($usuarios as $usuario) {
    			$usuario = $usuario;
    		}

    		return view('claves.nueva')->with('usuario', $usuario);
    	} else {

    		Flash::error('Los datos susministrados no coinciden con los de nuestros registros');
    		return redirect()->route('claves.restore');
    	}
    }

    public function actualizar(actualizarUser $request, $id)
    {
        $usuario = User::find($id);

        $usuario->password = bcrypt($request->clave);

        $usuario->save();

        Flash::success('La contraseña del usuario: '.$usuario->usuario. 'ha sido cambiada con exito');
        return redirect()->route('auth.login');
    }
}
