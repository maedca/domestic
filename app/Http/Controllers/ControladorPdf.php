<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contratante;
use App\Trabajador;

class ControladorPdf extends Controller
{

    public function pdfContratante($id)
    {
        $contratante = $this->getDataContratante($id);
        $view = \View::make('pdf.pdfcontratante', compact('contratante'))->render();
        $pdf = \App::make('dompdf.wrapper');

        $pdf->loadHTML($view);

        return $pdf->stream('pdfContratante');
    }

    public function getDataContratante($id)
    {
        $data = Contratante::find($id);

        return $data;
    }

    public function pdfTrabajador($id)
    {
        $trabajador = $this->getDataTrabajador($id);
        $view = \View::make('pdf.pdftrabajador', compact('trabajador'))->render();
        $pdf = \App::make('dompdf.wrapper');

        $pdf->loadHTML($view);

        return $pdf->stream('pdftrabajador');
    }

    public function getDataTrabajador($id)
    {
        $data = Trabajador::find($id);

        return $data;
    }
}
