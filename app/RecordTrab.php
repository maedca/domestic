<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecordTrab extends Model
{
    protected $table = "trab_records";

    protected $fillable = [ 'record', 'fecha', 'trabajador_id', ];

    public function trabajador()
    {
    	return $this->belongsTo('App\Trabajador');
    }
}
