<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $table = "ciudades";

    protected $fillable = [
        'ciudad',
    ];

    public function trabajadores()
    {
        return $this->hasMany('App\Trabajador');
    }

    public function contratantes()
    {
        return $this->hasMany('App\Contratante');
    }
}
