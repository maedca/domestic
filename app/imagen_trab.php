<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class imagen_trab extends Model
{
    protected $table = "imagenes_trab";

    protected $fillable = ['id', 'ruta', 'trabajador_id'];

    public function trabajador()
    {
    	return $this->belongsTo('App\Trabajador');
    }
}
