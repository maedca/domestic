<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecordContr extends Model
{
    protected $table = "contra_records";

    protected $fillable = [ 'record', 'fecha', 'contratante_id', ];

    public function contratante()
    {
    	return $this->belongsTo('App\Contratante');
    }
}
