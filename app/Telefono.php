<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telefono extends Model
{
    protected $table = "telefonos";

    protected $fillable = [
        'telefono', 'celular',
    ];

    public function trabajadores()
    {
        return $this->hasMany('App\Trabajador');
    }

    public function contratantes()
    {
        return $this->hasMany('App\Contratante');
    }
}
