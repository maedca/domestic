<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPredio extends Model
{
    protected $table = "tipos_predio";

    protected $fillable = [
        'predio',
    ];

    public function trabajadores()
    {
        return $this->hasMany('App\Trabajador');
    }

    public function contratantes()
    {
        return $this->hasMany('App\Contratante');
    }
}
