<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaRecord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contra_records', function(Blueprint $table){
            $table->increments('id');
            $table->string('record');
            $table->date('fecha');
            $table->integer('contratante_id')->unsigned();
            $table->timestamps();

            $table->foreign('contratante_id')->references('id')->on('contratantes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contra_records');
    }
}
