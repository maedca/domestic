<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreartablaContratantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratantes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombres');
            $table->string('direccion');
            $table->string('doc_ide');
            $table->integer('pers_atend');
            $table->integer('adultos');
            $table->integer('terc_edad');
            $table->integer('ninos');
            $table->integer('pisos');
            $table->integer('mascotas');
            $table->string('imagen');
            $table->string('Salario');
            $table->string('diasdescanso');
            $table->enum('ttrabajo',['Interna','Externa', 'Por dias']);
            $table->integer('ciudad_id')->unsigned();
            $table->integer('barrio_id')->unsigned();
            $table->integer('telefono_id')->unsigned()->nullable();
            $table->integer('predio_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('ciudad_id')->references('id')->on('ciudades')->onDelete('cascade');
            $table->foreign('barrio_id')->references('id')->on('barrios')->onDelete('cascade');
            $table->foreign('telefono_id')->references('id')->on('telefonos')->onDelete('cascade');
            $table->foreign('predio_id')->references('id')->on('tipos_predio')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contratantes');
    }
}
