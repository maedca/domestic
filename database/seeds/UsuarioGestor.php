<?php

use Illuminate\Database\Seeder;

class UsuarioGestor extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
	        'user'         => 'gestor',
	        'password'     => bcrypt('gestor'),
            'correo'       => 'gestor@example.com',
	        'tipo'         => 'gestor',
        ]);
    }
}
