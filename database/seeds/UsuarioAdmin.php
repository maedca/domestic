<?php

use Illuminate\Database\Seeder;

class UsuarioAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
	        'user'         => 'admin',
	        'password'     => bcrypt('1098625584'),
            'correo'       => 'admin@example.com',
	        'tipo'         => 'administrador',
        ]);
    }
}
