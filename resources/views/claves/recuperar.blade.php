@extends('plantillas.default.default')

@section('titulo', 'Recuperar Contraseña. Paso 1')

@section('contenido')
	<div class="col-sm-10 col-sm-offset-1">
		<h2 class="text-center">Recuperar contraseña</h2>
		{!! Form::open(['route' => 'claves.verificar', 'method' => 'POST', 'class' => 'col-sm-4 col-sm-offset-4']) !!}
			<div class="form-group">
				{!! Form::label('usuario', 'Usuario') !!}
				{!! Form::text('usuario', null, ['class' => 'form-control', 'placeholder' => 'Ingrese su usuario', 'required']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('correo', 'Correo') !!}
				{!! Form::email('correo', null, ['class' => 'form-control', 'placeholder' => 'Ingrese su correo', 'required']) !!}
			</div>
			<div class="form-group">
				<input type="submit" name="recuperar" value="Recuperar" class="btn btn-personal">
				<a href="/" class="btn btn-personal">Inicio</a>
			</div>
		{!! Form::close() !!}
	</div>
@endsection