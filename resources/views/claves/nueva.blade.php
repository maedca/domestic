@extends('plantillas.default.default')

@section('titulo', 'Nueva contraseña')

@section('contenido')
	<div class="col-sm-10 col-sm-offset-1">
		<h2 class="text-center">Nueva contraseña</h2>
		{!! Form::open(['route' => ['claves.actualizar', $usuario], 'method' => 'PUT', 'class' => 'col-sm-4 col-sm-offset-4']) !!}
			<div class="form-group">
				{!! Form::label('usuario', 'Usuario') !!}
				{!! Form::text('usuario', $usuario->user, ['class' => 'form-control', 'placeholder' => 'Ingrese su usuario', 'required', 'readonly']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('correo', 'Correo') !!}
				{!! Form::email('correo', $usuario->correo, ['class' => 'form-control', 'placeholder' => 'Ingrese su correo', 'required', 'readonly']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('clave', 'Nueva Clave') !!}
				{!! Form::password('clave', ['class' => 'form-control', 'placeholder' => 'Ingrese la contraseña' ,'required']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('rep_clave', 'Repetir la nueva clave') !!}
				{!! Form::password('rep_clave', ['class' => 'form-control', 'placeholder' => 'Repita la contraseña' , 'required']) !!}
			</div>
			<div class="form-group">
				<input type="submit" name="recuperar" value="Actualizar" class="btn btn-personal">
				<a href="/" class="btn btn-personal">Inicio</a>
			</div>
		{!! Form::close() !!}
	</div>
@endsection