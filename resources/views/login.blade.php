@extends('plantillas.default.default')

@section('contenido')
	<div class="col-sm-offset-4 col-sm-4">
		<img src="{{ asset('img/logo.png') }}" class="center-block">
		<div class="text-center">
			<h4>
				Inicio de Sesion
			</h4>
		</div>
		{!! Form::open(['route' => 'auth.login', 'method' => 'POST']) !!}
			<div class="form-group">
				<label for="user">Usuario: </label>
				<input class="form-control" type="text" name="user" placeholder="Ingrese su Usuario" required>
			</div>
			<div class="form-group">
				<label for="password">Contraseña: </label>
				<input class="form-control" type="password" name="password" placeholder="Ingrese su Contraseña" required>
			</div>
			<div class="form-group">
				<input type="submit" name="login" value="Entrar" class="form-control btn-personal">
			</div>
		{!! Form::close() !!}
	</div>
@endsection
