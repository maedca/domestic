@extends('plantillas.default.default')

@section('contenido')
	<div class="col-sm-offset-2 col-sm-8">
		<img src="{{ asset('img/logo.png') }}" class="pull-left">
		<br>
		@if(Auth::user()->tipo == 'administrador')
			<div class="pull-right text-center">
				{!! Form::open(['route' => 'buscar', 'method' => 'GET', 'target' => '_blank']) !!}
		            <p>BUSQUEDA ID</p>
		            <input type="text" name="busqueda" value="{{ $trabajador->doc_ide }}">
		        {!! Form::close() !!}
			</div>
		@endif
		<div class="col-sm-offset-1 col-sm-10">
			<h2 class="text-center">Hoja de vida trabajador</h2>
			<div class="col-sm-10">
				<p class="text-justify"><strong>Nombres y Apellidos: </strong><br>
				{{ $trabajador->nombres }}</p>
				<p class="text-justify"><strong>Direccion: </strong><br>
				{{ $trabajador->direccion }}</p>
				<p class="text-justify"><strong>Barrio: </strong><br>
				{{ $trabajador->barrio->barrios }}</p>
				<p class="text-justify"><strong>Ciudad: </strong><br>
				{{ $trabajador->ciudad->ciudad }}</p>
				<p class="text-justify"><strong>Documento de Identidad: </strong><br>
				{{ $trabajador->doc_ide }}</p>
				<p class="text-justify"><strong>Telefono: </strong><br>
					@if( $trabajador->telefono_id == null )
						No
					@else
						@if($trabajador->telefono->telefono == null)
							No
						@else
							{{ $trabajador->telefono->telefono }}
						@endif
					@endif
				</p>
				<p class="text-justify"><strong>Celular: </strong><br>
					@if( $trabajador->telefono_id == null )
						No
					@else
						@if($trabajador->telefono->celular == null)
							No
						@else
							{{ $trabajador->telefono->celular }}
						@endif
					@endif
				</p>
				<p class="text-justify"><strong>Fecha de Nacimiento: </strong><br>
				{{ $trabajador->fech_naci }}</p>
				<p class="text-justify"><strong>Lugar de Nacimiento: </strong><br>
				{{ $trabajador->lugar_naci }}</p>
				<h3 class="text-center">Record</h3>
				<p class="text-justify">
					@foreach($trabajador->records as $record)
						<strong>{{ $record->fecha }}</strong><br>
						{{ $record->record }}<br>
					@endforeach
				</p>
			</div>
			@foreach($trabajador->imagenes as $imagen)
				<div class="col-sm-2">
					<img src="{{ asset($imagen->ruta) }}"  class="img-rounded img-personal derecha">
				</div>
			@endforeach
			<p class="text-justify col-sm-12">
				<a href="{{ route('trabajadores.pdf', $trabajador->id) }}" class="btn btn-personal" target="_blank">PDF</a>
				@if(Auth::user()->tipo == 'administrador')
					<a href="{{ route('trabajadores.edit', $trabajador->id) }}" class="btn btn-personal">Editar</a>
					<a href="{{ route('trabajadores.eliminar', $trabajador->id) }}" class="btn btn-personal" onclick="return confirm('¿Desea eliminar este Trabajador?')">Eliminar</a>
				@endif
				<a href="{{ route('home') }}" class="btn btn-personal">Inicio</a>
			</p>
		</div>
	</div>
@endsection