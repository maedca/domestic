@extends('plantillas.default.default')

@section('titulo', 'Nueva hoja de vida')

@section('contenido')
	<div class="col-sm-offset-2 col-sm-8">
		<img src="{{ asset('img/logo.png') }}" class="pull-left">
		<br>
		@if(Auth::user()->tipo == 'administrador')
			<div class="pull-right text-center">
				{!! Form::open(['route' => 'buscar', 'method' => 'GET', 'target' => '_blank']) !!}
		            <p>BUSQUEDA ID</p>
		            <input type="text" name="busqueda">
		        {!! Form::close() !!}
			</div>
		@endif
		<div class="col-sm-12">
			{!! Form::open(['route' => 'trabajadores.store', 'method' => 'POST', 'files' => true]) !!}
				<h2 class="text-center">Curriculum de Trabajo</h2>
				<div class="form-group pull-right">
					<label>Fecha</label>
					<input name="fecha" type="date" value="{{ date('Y-m-d') }}" readonly required>
				</div>
				<div class="form-group col-sm-12">
					<label>Nombres y Apellidos</label>
					<input type="text" name="nombres" required class="form-control" placeholder="Ingresa tu nombre y apellido">
				</div>
				<div class="form-group col-sm-6">
					<label>Direccion</label>
					<input type="text" name="direccion" required class="form-control" placeholder="Ingresa tu direccion">
				</div>
				<div class="form-group col-sm-6">
					<label>Ciudad</label>
					{!! Form::select('ciudad', $ciudades, null, ['class' => 'form-control', 'placeholder' => 'Seleccione su ciudad', 'required']) !!}
				</div>
				<div class="form-group col-sm-6">
					<label>Barrio</label>
					<input type="text" name="barrio" required class="form-control" placeholder="Ingresa tu barrio">
				</div>
				<div class="form-group col-sm-6">
					<label>Documento de Identidad</label>
					<input type="text" name="documento" required class="form-control" placeholder="Ingresa tu documento de identidad">
				</div>
				<div class="form-group col-sm-6">
					<label>Telefono</label>
					<input type="text" name="telefono" required class="form-control" placeholder="Ingresa tu numero telefonico">
				</div>
				<div class="form-group col-sm-6">
					<label>Celular</label>
					<input type="text" name="celular" required class="form-control" placeholder="Ingresa el numero de tu movil">
				</div>
				<div class="form-group col-sm-6">
					<label>Fecha de nacimiento AAAA/MM/DD</label>
					<input type="date" name="fechnac" required class="form-control" size="15" placeholder="Año - Mes - Dia">
				</div>
				<div class="form-group col-sm-6">
					<label>Lugar de nacimiento</label>
					<input type="text" name="lugar" required class="form-control" size="15" placeholder="Ingresa tu lugar de nacimeinto">
				</div>
				<div class="form-group col-sm-12">
					<label>Gestion</label>
					<textarea class="form-control" name="gestion" required placeholder="Ingresa tu gestion"></textarea>
				</div>
				<div class="form-group col-sm-12">
					<h4 class="text-center titulosilver">Record</h4>
					<textarea class="form-control" name="historial" rows="15" required readonly disabled></textarea>
				</div>
				<div class="form-group col-sm-6">
					<label>Subir Imagen</label>
					<input type="file" name="imagenes[]" multiple>
					<input type="file" name="imagenes[]" multiple>
					<input type="file" name="imagenes[]" multiple>
					<input type="file" name="imagenes[]" multiple>
					<input type="file" name="imagenes[]" multiple>
					<p class="text-justify">Las imagenes deben poseer las siguientes dimensiones: 150px de Ancho por 150px de Alto</p>
				</div>
				<div class="form-group col-sm-6 text-right">
					<a href="{{ route('home') }}" class="btn btn-personal">Inicio</a>
					<input type="submit" name="registrar" value="Guardar" class="btn btn-personal">
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection
