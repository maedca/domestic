@extends('plantillas.default.default')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/css/jquery.datatables.css') }}">
@endsection

@section('contenido')
	<div class="col-sm-offset-1 col-sm-10 text-justify">
		<h2 class="text-center">Listado de Empleados</h2>
		<table id="listrabajadores" class="table table-hover">
			<thead>
				<tr>
					<th>Nombre y Apellido</th>
					<th>Edad (años)</th>
					<th>Ciudad</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($trabajadores as $trabajador)
					<tr>
						<td>{{ $trabajador->nombres }}</td>
						<td>{{ $trabajador->edad($trabajador->fech_naci) }}</td>
						<td>{{ $trabajador->ciudad->ciudad }}</td>
						<td><a href="{{ route('trabajadores.show', $trabajador) }}" class="btn btn-personal" target="_blank">Ver</a></td>
					</tr>
				@endforeach
			</tbody>
		</table>

		<p class="text-justify">
			<a href="{{ route('home') }}" class="btn btn-personal">Inicio</a>
		</p>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset('plugins/datatables/js/jquery.dataTables.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
		    $('#listrabajadores').DataTable({
		    	"language": {
		            "lengthMenu": "Ver _MENU_ entradas por pagina",
		            "zeroRecords": "No se encontraron resultados",
		            "info": "Viendo la pagina _PAGE_ de _PAGES_",
		            "infoEmpty": "No hay informacion",
		            "search": "Buscar: ",
		            "paginate": {
				        "previous": "Anterior ",
				        "next": " Proximo",
				    }
		        }
		    });
		} );
	</script>
@endsection