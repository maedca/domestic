@extends('plantillas.default.default')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/css/jquery.datatables.css') }}">
@endsection

@section('contenido')
	<div class="col-sm-offset-1 col-sm-10">
		<h2 class="text-center">Gestor de Usuarios</h2>
		<table id="listusuarios" class="table table-hover text-center">
			<thead>
				<tr>
					<th>Usuario</th>
					<th>Fecha</th>
					<th>Estatus</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($usuarios as $usuario)
					<tr>
						<td>{{ $usuario->user }}</td>
						<td>{{ $usuario->created_at->toDateString() }}</td>
						<td>{{ $usuario->tipo }}</td>
						<td>
							<a href="#" class="btn btn-personal" data-toggle="modal" data-target="#{{$usuario->user}}">Editar</a>
							<a href="{{ route('users.destroy', $usuario) }}" class="btn btn-personal" onclick="return confirm('¿Desea eliminar este Usuario?')">Eliminar</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<p class="text-justify">
			<a href="{{ route('home') }}" class="btn btn-personal">Inicio</a>
			<a href="#" class="btn btn-personal" data-toggle="modal" data-target="#regusuario">Nuevo</a>
		</p>
	</div>
	@foreach($usuarios as $usuario)
		<div class="modal fade" id="{{$usuario->user}}" tabindex="-1" role="dialog" aria-labelledby="{{$usuario->user}}">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="{{$usuario->user}}">Editar Usuario: {{ $usuario->user }}</h4>
					</div>
					<div class="modal-body">
						{!! Form::open(['route' => ['users.update', $usuario], 'method' => 'PUT']) !!}
							<div class="form-group">
								{!! Form::label('usuario', 'Usuario') !!}
								{!! Form::text('usuario', $usuario->user, ['class' => 'form-control']) !!}
							</div>
							<div class="form-group">
								{!! Form::label('tusuario', 'Estatus') !!}
								{!! Form::select('tusuario', ['administrador' => 'Administrador', 'gestor' => 'Gestor'], $usuario->tipo, ['class' => 'form-control']) !!}
							</div>
							<div class="form-group">
								{!! Form::label('correo', 'Email') !!}
								{!! Form::email('correo', $usuario->correo, ['class' => 'form-control']) !!}
							</div>
							<div class="form-group">
								{!! Form::label('clave', 'Nueva Contraseña') !!}
								{!! Form::password('clave', ['class' => 'form-control']) !!}
							</div>
							<div class="form-group">
								{!! Form::label('clave2', 'Repetir Contraseña') !!}
								{!! Form::password('clave2', ['class' => 'form-control']) !!}
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
								<button type="submit" class="btn btn-primary">Guardar</button>
							</div>
						{!! Form::close() !!}
					</div>
				</div>	
			</div>
		</div>
	@endforeach
	<div class="modal fade" id="regusuario" tabindex="-1" role="dialog" aria-labelledby="regusuario">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="regusuario">Registrar nuevo usuario</h4>
				</div>
				<div class="modal-body">
					{!! Form::open(['route' => 'users.store', 'method' => 'POST']) !!}
						<div class="form-group">
							{!! Form::label('usuario', 'Usuario') !!}
							{!! Form::text('usuario', null, ['class' => 'form-control', 'placeholder' => 'Ingerese el usuario', 'required']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('tusuario', 'Estatus') !!}
							{!! Form::select('tusuario', ['administrador' => 'Administrador', 'gestor' => 'Gestor'], null, ['class' => 'form-control', 'placeholder' => 'Seleccione una opcion', 'required']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('correo', 'Email') !!}
							{!! Form::email('correo', null, ['class' => 'form-control', 'placeholder' => 'Ingrese el E-mail', 'required']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('clave', 'Clave') !!}
							{!! Form::password('clave', ['class' => 'form-control', 'placeholder' => 'Ingrese la contraseña' ,'required']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('rep_clave', 'Repetir Clave') !!}
							{!! Form::password('rep_clave', ['class' => 'form-control', 'placeholder' => 'Repita la contraseña' , 'required']) !!}
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
							<button type="submit" class="btn btn-primary">Guardar</button>
						</div>
					{!! Form::close() !!}
				</div>
			</div>	
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset('plugins/datatables/js/jquery.dataTables.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
		    $('#listusuarios').DataTable({
		    	"language": {
		            "lengthMenu": "Ver _MENU_ entradas por pagina",
		            "zeroRecords": "No se encontraron resultados",
		            "info": "Viendo la pagina _PAGE_ de _PAGES_",
		            "infoEmpty": "No hay informacion",
		            "search": "Buscar: ",
		            "paginate": {
				        "previous": "Anterior ",
				        "next": " Proximo",
				    }
		        }
		    });
		} );
	</script>
@endsection