@extends('plantillas.default.default')

@section('titulo', 'Resultado de la Busqueda')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/css/jquery.datatables.css') }}">
@endsection

@section('contenido')

	<h2 class="text-center">Resultado de la busqueda</h2>
	<div class="col-sm-offset-1 col-sm-10">
		<div class="pull-right">
			<p class="text-center">
				BUSQUEDA ID
			</p>
			{!! Form::open(['route' => 'buscar', 'method' => 'POST', 'class' => 'col-sm-3']) !!}
				<input type="text" name="busqueda">
			{!! Form::close() !!}
		</div>
		<div class="col-sm-12">
			<h3 class="text-center">Trabajadores</h3>
			<hr>
			<table id="listrabajadores" class="table table-hover">
				<thead>
					<tr>
						<th>Nombre y Apellido</th>
						<th>Edad (años)</th>
						<th>Ciudad</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($trabajadores as $trabajador)
						<tr>
							<td>{{ $trabajador->nombres }}</td>
							<td>{{ $trabajador->edad($trabajador->fech_naci) }}</td>
							<td>{{ $trabajador->ciudad->ciudad }}</td>
							<td><a href="{{ route('trabajadores.show', $trabajador) }}" class="btn btn-personal" target="_blank">Ver</a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<h3 class="text-center">Contratantes</h3>
			<hr>
			<table id="liscontratantes" class="table table-hover">
				<thead>
					<tr>
						<th>Nombre y Apellido</th>
						<th>Personas a Atender</th>
						<th>Predio</th>
						<th>Pisos</th>
						<th>Salario</th>
						<th>Modo de Trabajo</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($contratantes as $contratante)
						<tr>
							<td>{{ $contratante->nombres }}</td>
							<td>{{ $contratante->pers_atend }}</td>
							<td>{{ $contratante->predio->predio }}</td>
							<td>{{ $contratante->pisos }}</td>
							<td>{{ $contratante->Salario }}</td>
							<td>{{ $contratante->ttrabajo }}</td>
							<td><a href="{{ route('contratantes.show', $contratante) }}" class="btn btn-personal" target="_blank">Ver</a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<p class="text-justify col-sm-12">
			<a href="{{ route('home') }}" class="btn btn-personal">Inicio</a>
		</p>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset('plugins/datatables/js/jquery.dataTables.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
		    $('#listrabajadores').DataTable({
		    	"language": {
		            "lengthMenu": "Ver _MENU_ entradas por pagina",
		            "zeroRecords": "No se encontraron resultados",
		            "info": "Viendo la pagina _PAGE_ de _PAGES_",
		            "infoEmpty": "No hay informacion",
		            "search": "Buscar: ",
		            "paginate": {
				        "previous": "Anterior ",
				        "next": " Proximo",
				    }
		        }
		    });
		    $('#liscontratantes').DataTable({
		    	"language": {
		            "lengthMenu": "Ver _MENU_ entradas por pagina",
		            "zeroRecords": "No se encontraron resultados",
		            "info": "Viendo la pagina _PAGE_ de _PAGES_",
		            "infoEmpty": "No hay informacion",
		            "search": "Buscar: ",
		            "paginate": {
				        "previous": "Anterior ",
				        "next": " Proximo",
				    }
		        }
		    });
		} );
	</script>
@endsection