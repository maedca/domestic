@extends('plantillas.default.default')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/css/jquery.datatables.css') }}">
@endsection

@section('contenido')
	<div class="col-sm-offset-1 col-sm-10 text-justify">
		<h2 class="text-center">Listado de Empleadores</h2>
		<table id="liscontratantes" class="table table-hover">
			<thead>
				<tr>
					<th>Nombre y Apellido</th>
					<th>Personas a Atender</th>
					<th>Predio</th>
					<th>Pisos</th>
					<th>Salario</th>
					<th>Modo de Trabajo</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($contratantes as $contratante)
					<tr>
						<td>{{ $contratante->nombres }}</td>
						<td>{{ $contratante->pers_atend }}</td>
						<td>{{ $contratante->predio->predio }}</td>
						<td>{{ $contratante->pisos }}</td>
						<td>{{ $contratante->Salario }}</td>
						<td>{{ $contratante->ttrabajo }}</td>
						<td><a href="{{ route('contratantes.show', $contratante) }}" class="btn btn-personal" target="_blank">Ver</a></td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<p class="text-justify">
			<a href="{{ route('home') }}" class="btn btn-personal">Inicio</a>
		</p>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset('plugins/datatables/js/jquery.dataTables.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
		    $('#liscontratantes').DataTable({
		    	"language": {
		            "lengthMenu": "Ver _MENU_ entradas por pagina",
		            "zeroRecords": "No se encontraron resultados",
		            "info": "Viendo la pagina _PAGE_ de _PAGES_",
		            "infoEmpty": "No hay informacion",
		            "search": "Buscar: ",
		            "paginate": {
				        "previous": "Anterior ",
				        "next": " Proximo",
				    }
		        }
		    });
		} );
	</script>
@endsection