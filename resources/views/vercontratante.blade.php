@extends('plantillas.default.default')

@section('contenido')
	<div class="col-sm-offset-2 col-sm-8">
		<img src="{{ asset('img/logo.png') }}" class="pull-left">
		<br>
		@if(Auth::user()->tipo == 'administrador')
			<div class="pull-right text-center">
				{!! Form::open(['route' => 'buscar', 'method' => 'GET', 'target' => '_blank']) !!}
		            <p>BUSQUEDA ID</p>
		            <input type="text" name="busqueda" value="{{ $contratante->doc_ide }}">
		        {!! Form::close() !!}
			</div>
		@endif
		<div class="col-sm-offset-1 col-sm-10">
			<h2 class="text-center">Record contratante</h2>
			<div class="col-sm-10">
				<p class="text-justify"><strong>Nombres y Apellidos: </strong><br>
				{{ $contratante->nombres }}</p>
				<p class="text-justify"><strong>Direccion: </strong><br>
				{{ $contratante->direccion }}</p>
				<p class="text-justify"><strong>Barrio: </strong><br>
				{{ $contratante->barrio->barrios }}</p>
				<p class="text-justify"><strong>Ciudad: </strong><br>
				{{ $contratante->ciudad->ciudad }}</p>
				<p class="text-justify"><strong>Documento de Identidad: </strong><br>
				{{ $contratante->doc_ide }}</p>
				<p class="text-justify"><strong>Tipo de predio: </strong><br>
				{{ $contratante->predio->predio }}</p>
				<p class="text-justify"><strong>Telefono: </strong><br>
					@if($contratante->telefono_id == null)
						No
					@else
						{{ $contratante->telefono->telefono }}	 
					@endif
				</p>
				<p class="text-justify"><strong>Celular: </strong><br>
					@if($contratante->telefono_id == null)
						No
					@else
						{{ $contratante->telefono->celular }}	 
					@endif
				</p>
				<p class="text-justify"><strong>Personas a atender: </strong><br>
				{{ $contratante->pers_atend }}</p>
				<p class="text-justify"><strong>Adultos: </strong><br>
				{{ $contratante->adultos }}</p>
				<p class="text-justify"><strong>Tercera edad: </strong><br>
				{{ $contratante->terc_edad }}</p>
				<p class="text-justify"><strong>Niños: </strong><br>
				{{ $contratante->ninos }}</p>
				<p class="text-justify"><strong>Mascotas: </strong><br>
				{{ $contratante->mascotas }}</p>
				<p class="text-justify"><strong>Pisos: </strong><br>
				{{ $contratante->pisos }}</p>
				<p class="text-justify"><strong>Salario: </strong><br>
				{{ $contratante->Salario }}</p>
				<p class="text-justify"><strong>Dias de Descanso: </strong><br>
				{{ $contratante->diasdescanso }}</p>
				<p class="text-justify"><strong>Forma de Trabajo: </strong><br>
				{{ $contratante->ttrabajo }}</p>
				<h3 class="text-center">Record</h3>
				<p class="text-justify">
					@foreach($contratante->records as $record)
						<strong>{{ $record->fecha }}</strong><br>
						{{ $record->record }}<br>
					@endforeach
				</p>
			</div>
			<div class="col-sm-2">
				<img src="{{ asset($contratante->imagen) }}" class="img-rounded img-personal">
			</div>
			<p class="text-justify col-sm-12">
				<a href="{{ route('contratantes.pdf', $contratante->id) }}" class="btn btn-personal" target="_blank">PDF</a>
				@if(Auth::user()->tipo == 'administrador')
					<a href="{{ route('contratantes.edit', $contratante->id) }}" class="btn btn-personal">Editar</a>
					<a href="{{ route('contratantes.eliminar', $contratante->id) }}" class="btn btn-personal" onclick="return confirm('¿Desea eliminar este Contratante?')">Eliminar</a>
				@endif
				<a href="{{ route('home') }}" class="btn btn-personal">Inicio</a>
			</p>
		</div>
	</div>
@endsection