<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Documento</title>
		<link rel="stylesheet" type="text/css" href="./plugins/bootstrap/css/bootstrap2.css">
	</head>
	<body>
		<br>
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-offset-2 col-sm-8">
					<img src="./img/logo.png" class="pull-left">
				</div>
				<div>
					<h2 class="text-center">Record contratante</h2>
				</div>
					<div class="col-sm-offset-1 col-sm-10">
						<img src="./{{ $contratante->imagen }}" class="img-rounded img-personal derecha">
						<div class="col-sm-10 arriba">
							<p class="text-justify"><strong>Nombres y Apellidos: </strong><br>
							{{ $contratante->nombres }}</p>
							<p class="text-justify"><strong>Direccion: </strong><br>
							{{ $contratante->direccion }}</p>
							<p class="text-justify"><strong>Barrio: </strong><br>
							{{ $contratante->barrio->barrios }}</p>
							<p class="text-justify"><strong>Ciudad: </strong><br>
							{{ $contratante->ciudad->ciudad }}</p>
							<p class="text-justify"><strong>Documento de Identidad: </strong><br>
							{{ $contratante->doc_ide }}</p>
							<p class="text-justify"><strong>Tipo de predio: </strong><br>
							{{ $contratante->predio->predio }}</p>
							<p class="text-justify"><strong>Telefono: </strong><br>
								@if($contratante->telefono_id == null)
									No
								@else
									{{ $contratante->telefono->telefono }}	 
								@endif
							</p>
							<p class="text-justify"><strong>Celular: </strong><br>
								@if($contratante->telefono_id == null)
									No
								@else
									{{ $contratante->telefono->celular }}	 
								@endif
							</p>
							<p class="text-justify"><strong>Personas a atender: </strong><br>
							{{ $contratante->pers_atend }}</p>
							<p class="text-justify"><strong>Adultos: </strong><br>
							{{ $contratante->adultos }}</p>
							<p class="text-justify"><strong>Tercera edad: </strong><br>
							{{ $contratante->terc_edad }}</p>
							<p class="text-justify"><strong>Niños: </strong><br>
							{{ $contratante->ninos }}</p>
							<p class="text-justify"><strong>Mascotas: </strong><br>
							{{ $contratante->mascotas }}</p>
							<p class="text-justify"><strong>Pisos: </strong><br>
							{{ $contratante->pisos }}</p>
							<p class="text-justify"><strong>Salario: </strong><br>
							{{ $contratante->Salario }}</p>
							<br>
							<br>
							<p class="text-justify"><strong>Dias de Descanso: </strong><br>
							{{ $contratante->diasdescanso }}</p>
							<p class="text-justify"><strong>Forma de Trabajo: </strong><br>
							{{ $contratante->ttrabajo }}</p>
						</div>
						<div>
							<h3 class="text-center">Record</h3>
							<p class="text-justify">
								@foreach($contratante->records as $record)
									<strong>{{ $record->fecha }}</strong><br>
									{{ $record->record }}<br>
								@endforeach
							</p>
						</div>
					</div>
			</div>
		</div>
	</body>
</html>