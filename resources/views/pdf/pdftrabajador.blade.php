<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Documento</title>
		<link rel="stylesheet" type="text/css" href="./plugins/bootstrap/css/bootstrap2.css">
	</head>
	<body>
		<br>
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-8">
					<img src="./img/logo.png" class="pull-left">
				</div>
				<div>
					<h2 class="text-center">Hoja de vida trabajador</h2>
				</div>
				<div class="col-sm-offset-1 col-sm-10">
					<img src="./{{ $trabajador->imagenes->first()->ruta }}"  class="img-rounded img-personal derecha">
					<div class="col-sm-8 arribatrab">
						<p class="text-justify"><strong>Nombres y Apellidos: </strong><br>
						{{ $trabajador->nombres }}</p>
						<p class="text-justify"><strong>Direccion: </strong><br>
						{{ $trabajador->direccion }}</p>
						<p class="text-justify"><strong>Barrio: </strong><br>
						{{ $trabajador->barrio->barrios }}</p>
						<p class="text-justify"><strong>Ciudad: </strong><br>
						{{ $trabajador->ciudad->ciudad }}</p>
						<p class="text-justify"><strong>Documento de Identidad: </strong><br>
						{{ $trabajador->doc_ide }}</p>
						<p class="text-justify"><strong>Telefono: </strong><br>
							@if( $trabajador->telefono_id == null )
								No
							@else
								@if($trabajador->telefono->telefono == null)
									No
								@else
									{{ $trabajador->telefono->telefono }}
								@endif
							@endif
						</p>
						<p class="text-justify"><strong>Celular: </strong><br>
							@if( $trabajador->telefono_id == null )
								No
							@else
								@if($trabajador->telefono->celular == null)
									No
								@else
									{{ $trabajador->telefono->celular }}
								@endif
							@endif
						</p>
						<p class="text-justify"><strong>Fecha de Nacimiento: </strong><br>
						{{ $trabajador->fech_naci }}</p>
						<p class="text-justify"><strong>Lugar de Nacimiento: </strong><br>
						{{ $trabajador->lugar_naci }}</p>
					</div>
					<div>
						<h3 class="text-center">Record</h3>
						<p class="text-justify">
							@foreach($trabajador->records as $record)
								<strong>{{ $record->fecha }}</strong><br>
								{{ $record->record }}<br>
							@endforeach
						</p>
					</div>
					@foreach($trabajador->imagenes as $imagen)
						@if($imagen->ruta != $trabajador->imagenes->first()->ruta)
							<img src="./{{ $imagen->ruta }}"  class="img-rounded img-personal">
						@endif
					@endforeach
				</div>
			</div>
		</div>
	</body>
</html>