@extends('plantillas.default.default')

@section('contenido')
	<div class="col-sm-offset-2 col-sm-8">
		<img src="{{ asset('img/logo.png') }}" class="pull-left">
		<br>
		@if(Auth::user()->tipo == 'administrador')
			<div class="pull-right text-center">
				{!! Form::open(['route' => 'buscar', 'method' => 'GET', 'target' => '_blank']) !!}
		            <p>BUSQUEDA ID</p>
		            <input type="text" name="busqueda" value="{{ $contratante->doc_ide }}">
		        {!! Form::close() !!}
			</div>
		@endif
		<div class="col-sm-12">
			{!! Form::open(['route' => ['contratantes.update', $contratante], 'method' => 'PUT', 'files' => true]) !!}
				<h2 class="text-center">Contratante</h2>
				<div class="form-group pull-right">
					<label>Fecha</label>
					<input name="fecha" type="date" value="{{ date('Y-m-d') }}" readonly required>
				</div>
				<div class="form-group col-sm-12">
					<label>Nombres y Apellidos</label>
					<input type="text" name="nombres" value="{{ $contratante->nombres }}" required class="form-control">
				</div>
				<div class="form-group col-sm-6">
					<label>Direccion</label>
					<input type="text" name="direccion" value="{{ $contratante->direccion }}" required class="form-control">
				</div>
				<div class="form-group col-sm-6">
					<label>Ciudad</label>
					{!! Form::select('ciudad', $ciudades, $contratante->ciudad_id, ['class' => 'form-control', 'required']) !!}
				</div>
				<div class="form-group col-sm-6">
					<label>Barrio</label>
					<input type="text" name="barrio" value="{{ $contratante->barrio->barrios }}" required class="form-control">
				</div>
				<div class="form-group col-sm-6">
					<label>Documento de Identidad</label>
					<input type="text" name="documento" value="{{ $contratante->doc_ide }}" required class="form-control">
				</div>
				<div class="form-group col-sm-6">
					<label>Telefono</label>
					@if($contratante->telefono_id == null)
						<input type="text" name="telefono" required class="form-control">
					@else
						@if($contratante->telefono->telefono == null)
							<input type="text" name="telefono" required class="form-control">
						@else
							<input type="text" name="telefono" value="{{ $contratante->telefono->telefono }}" required class="form-control">
						@endif	 
					@endif
				</div>
				<div class="form-group col-sm-6">
					<label>Celular</label>
					@if($contratante->telefono_id == null)
						<input type="text" name="celular" required class="form-control">
					@else
						@if($contratante->telefono->celular == null)
							<input type="text" name="celular" required class="form-control">
						@else
							<input type="text" name="celular" value="{{ $contratante->telefono->celular }}" required class="form-control">
						@endif	 
					@endif
				</div>
				<div class="form-group col-sm-6">
					<label>Tipo de predio</label>
					<input type="text" name="tpredio" value="{{ $contratante->predio->predio }}" required class="form-control" size="15">
				</div>
				<div class="form-group col-sm-3">
					<label>Personas a atender</label>
					<input type="text" name="personas" value="{{ $contratante->pers_atend }}" required class="form-control">
				</div>
				<div class="form-group col-sm-3">
					<label>Adultos</label>
					<input type="text" name="adultos" value="{{ $contratante->adultos }}" required class="form-control">
				</div>
				<div class="form-group col-sm-5">
					<label>Pisos</label>
					<input type="text" name="pisos" value="{{ $contratante->pisos }}" required class="form-control">
				</div>
				<div class="form-group col-sm-3">
					<label>Mascotas</label>
					<input type="text" name="mascotas" value="{{ $contratante->mascotas }}" required class="form-control">
				</div>
				<div class="form-group col-sm-2">
					<label>Tercera Edad</label>
					<input type="text" name="tedad" value="{{ $contratante->terc_edad }}" required class="form-control">
				</div>
				<div class="form-group col-sm-2">
					<label>Niños</label>
					<input type="text" name="ninos" value="{{ $contratante->ninos }}" required class="form-control">
				</div>
				<div class="form-group col-sm-6">
					<label>Salario</label>
					<input type="text" name="salario" value="{{ $contratante->Salario }}" required class="form-control">
				</div>
				<div class="form-group col-sm-6">
					<label>Dias de descanso</label>
					<input type="text" name="ddescanso" value="{{ $contratante->diasdescanso }}" required class="form-control">
				</div>
				<div class="form-group col-sm-12 text-center">
					<label>
						Interna <input type="radio" name="ttrabajo" value="Interna">
					</label>
					<label>
						Externa <input type="radio" name="ttrabajo" value="Externa">
					</label>
					<label>
						Por dias <input type="radio" name="ttrabajo" value="Por dias">
					</label>
				</div>
				<div class="form-group col-sm-12">
					<label>Gestion</label>
					<textarea class="form-control" name="gestion">{{ $contratante->gestion }}</textarea>
				</div>
				<div class="form-group col-sm-12">
					<h4 class="text-center titulosilver">Historial</h4>
					<p class="text-justify">
						@foreach($contratante->records as $record)
							<strong>{{ $record->fecha }}</strong><br> 
							{{ $record->record }}<br> 
						@endforeach
					</p>
				</div>
				<div class="form-group col-sm-6">
					<label>Subir Imagen</label>
					<input type="file" name="imagen">
					<p class="text-justify">La imagen debe poseer las siguientes dimensiones: 150px de Ancho por 180px de Alto</p>
				</div>
				<div class="form-group col-sm-6 text-right">
					<a href="{{ route('home') }}" class="btn btn-personal">Inicio</a>
					<input type="submit" name="registrar" value="Guardar" class="btn btn-personal">
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection