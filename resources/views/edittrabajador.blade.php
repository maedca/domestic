@extends('plantillas.default.default')

@section('contenido')
	<div class="col-sm-offset-2 col-sm-8">
		<img src="{{ asset('img/logo.png') }}" class="pull-left">
		<br>
		@if(Auth::user()->tipo == 'administrador')
			<div class="pull-right text-center">
				{!! Form::open(['route' => 'buscar', 'method' => 'GET', 'target' => '_blank']) !!}
		            <p>BUSQUEDA ID</p>
		            <input type="text" name="busqueda" value="{{ $trabajador->doc_ide }}">
		        {!! Form::close() !!}
			</div>
		@endif
		<div class="col-sm-12">
			{!! Form::open(['route' => ['trabajadores.update', $trabajador->id], 'method' => 'PUT', 'files' => true]) !!}
				<h2 class="text-center">Curriculum de Trabajo</h2>
				<div class="form-group pull-right">
					<label>Fecha</label>
					<input name="fecha" type="date" value="{{ date('Y-m-d') }}" readonly required>
				</div>
				<div class="form-group col-sm-12">
					<label>Nombres y Apellidos</label>
					<input type="text" name="nombres" value="{{ $trabajador->nombres }}" required class="form-control">
				</div>
				<div class="form-group col-sm-6">
					<label>Direccion</label>
					<input type="text" name="direccion" value="{{ $trabajador->direccion }}" required class="form-control">
				</div>
				<div class="form-group col-sm-6">
					<label>Ciudad</label>
					{!! Form::select('ciudad', $ciudades, $trabajador->ciudad_id, ['class' => 'form-control', 'required']) !!}
				</div>
				<div class="form-group col-sm-6">
					<label>Barrio</label>
					<input type="text" name="barrio" value="{{ $trabajador->barrio->barrios }}" required class="form-control">
				</div>
				<div class="form-group col-sm-6">
					<label>Documento de Identidad</label>
					<input type="text" name="documento" value="{{ $trabajador->doc_ide }}" required class="form-control">
				</div>
				<div class="form-group col-sm-6">
					<label>Telefono</label>
					@if( $trabajador->telefono_id == null )
						<input type="text" name="telefono" class="form-control">
					@else
						@if($trabajador->telefono->telefono == null)
							<input type="text" name="telefono" class="form-control">
						@else
							<input type="text" name="telefono" value="{{ $trabajador->telefono->telefono }}" class="form-control">
						@endif
					@endif
				</div>
				<div class="form-group col-sm-6">
					<label>Celular</label>
					@if( $trabajador->telefono_id == null )
						<input type="text" name="celular" class="form-control">
					@else
						@if($trabajador->telefono->celular == null)
							<input type="text" name="celular" class="form-control">
						@else
							<input type="text" name="celular" value="{{ $trabajador->telefono->celular }}" class="form-control">
						@endif
					@endif
				</div>
				<div class="form-group col-sm-6">
					<label>Fecha de nacimiento DD/MM/AAAA</label>
					<input type="date" name="fechnac" value="{{ $trabajador->fech_naci }}" required class="form-control" size="15">
				</div>
				<div class="form-group col-sm-6">
					<label>Lugar de nacimiento</label>
					<input type="text" name="lugar" value="{{ $trabajador->lugar_naci }}" required class="form-control" size="15">
				</div>
				<div class="form-group col-sm-12">
					<label>Gestion</label>
					<textarea class="form-control" name="gestion"></textarea>
				</div>
				<div class="form-group col-sm-12">
					<h4 class="text-center titulosilver">Historial</h4>
					<p class="text-justify">
						@foreach($trabajador->records as $record)
							<strong>{{ $record->fecha }}</strong><br>
							{{ $record->record }}<br>
						@endforeach
					</p>
				</div>
				<div class="form-group col-sm-6">
					<label>Editar Imagenes</label>
					@for($i = 0; $i < count($trabajador->imagenes); $i++)
						<input type="file" name="imagenes[]" multiple>
					@endfor
				</div>
				@if(count($trabajador->imagenes) < 5)
					<div class="form-group col-sm-6">
						<label>Subir Imagenes nuevas</label>
						@for($i = count($trabajador->imagenes); $i < 5 ; $i++)
							<input type="file" name="nimagenes[]" multiple>
						@endfor
					</div>
				@endif	
				<div class="form-group col-sm-6 text-right">
					<a href="{{ route('home') }}" class="btn btn-personal">Inicio</a>
					<input type="submit" name="registrar" value="Guardar" class="btn btn-personal">
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection
	
