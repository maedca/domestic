@extends('plantillas.default.default')

@section('contenido')
    <div class="col-sm-offset-4 col-sm-4">
        <img src="{{ asset('img/logo.png') }}" class="center-block">
        <div class="text-center">
            @if(Auth::user()->tipo == 'administrador' || Auth::user()->tipo == 'gestor')
                <a href="{{ route('trabajadores.create') }}" class="btn btn-personal" target="_blank">Nueva Hoja de Vida</a>
                <a href="{{ route('contratantes.create') }}" class="btn btn-personal" target="_blank">Nuevo Empleador</a>
            @endif
        </div>
    </div>
    <br>
    <br>
    <div class="col-sm-1">
        <a href="{{ route('auth.logout') }}" class="btn btn-personal">
            Cerrar Sesion
        </a>
    </div>
    @if(Auth::user()->tipo == 'administrador')
        <div class="col-sm-offset-1 col-sm-8 text-center">
            {!! Form::open(['route' => 'buscar', 'method' => 'POST', 'class' => 'col-sm-3', 'target' => '_blank']) !!}
                <p>BUSQUEDA ID</p>
                <input type="text" name="busqueda">
            {!! Form::close() !!}
            <br>
            <a href="{{ route('trabajadores.index') }}" class="btn btn-personal" target="_blank">Listado de Empleados</a>
            <a href="{{ route('contratantes.index') }}" class="btn btn-personal" target="_blank">Listado de Empleadores</a>
            <a href="{{ route('users.create') }}" class="btn btn-personal" target="_blank">Gestor de Usuarios</a>
        </div>
    @endif
@endsection
