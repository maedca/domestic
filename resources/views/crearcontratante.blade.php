@extends('plantillas.default.default')

@section('titulo', 'Crear empleador')

@section('contenido')
	<div class="col-sm-offset-2 col-sm-8">
		<img src="{{ asset('img/logo.png') }}" class="pull-left">
		<br>
		@if(Auth::user()->tipo == 'administrador')
			<div class="pull-right text-center">
				{!! Form::open(['route' => 'buscar', 'method' => 'POST', 'target' => '_blank']) !!}
		            <p>BUSQUEDA ID</p>
		            <input type="text" name="busqueda">
		        {!! Form::close() !!}
			</div>
		@endif
		<div class="col-sm-12">
			{!! Form::open(['route' => 'contratantes.store', 'method' => 'POST', 'files' => true]) !!}
				<h2 class="text-center">Empleador</h2>
				<div class="form-group pull-right">
					<label>Fecha</label>
					<input name="fecha" type="date" value="{{ date('Y-m-d') }}" readonly required>
				</div>
				<div class="form-group col-sm-12">
					<label>Nombres y Apellidos</label>
					<input type="text" name="nombres" required class="form-control" placeholder="Ingrese sus nombres y apellidos">
				</div>
				<div class="form-group col-sm-6">
					<label>Direccion</label>
					<input type="text" name="direccion" required class="form-control" placeholder="Ingerse su direccion">
				</div>
				<div class="form-group col-sm-6">
					<label>Ciudad</label>
					{!! Form::select('ciudad', $ciudades, null, ['class' => 'form-control', 'placeholder' => 'Seleccione su ciudad', 'required']) !!}
				</div>
				<div class="form-group col-sm-6">
					<label>Barrio</label>
					<input type="text" name="barrio" required class="form-control" placeholder="Ingerse su barrio">
				</div>
				<div class="form-group col-sm-6">
					<label>Documento de Identidad</label>
					<input type="text" name="documento" required class="form-control" placeholder="Ingerse su documento de identificacion">
				</div>
				<div class="form-group col-sm-6">
					<label>Telefono</label>
					<input type="text" name="telefono" class="form-control" placeholder="Ingrese su numero telefonico">
				</div>
				<div class="form-group col-sm-6">
					<label>Celular</label>
					<input type="text" name="celular" class="form-control" placeholder="Ingrese su numero de movil">
				</div>
				<div class="form-group col-sm-6">
					<label>Tipo de predio</label>
					<input type="text" name="tpredio" required class="form-control" size="15" placeholder="Ingrese su predio">
				</div>
				<div class="form-group col-sm-3">
					<label>Personas a atender</label>
					<input type="text" name="personas" required class="form-control" placeholder="Ingrese cantidad de personas a atender">
				</div>
				<div class="form-group col-sm-3">
					<label>Adultos</label>
					<input type="text" name="adultos" required class="form-control" placeholder="Ingrese cantidad de adultos">
				</div>
				<div class="form-group col-sm-5">
					<label>Pisos</label>
					<input type="text" name="pisos" required class="form-control" placeholder="Ingrese cantidad de pisos">
				</div>
				<div class="form-group col-sm-3">
					<label>Mascotas</label>
					<input type="text" name="mascotas" required class="form-control" placeholder="Ingrese cantidad de mascotas">
				</div>
				<div class="form-group col-sm-2">
					<label>Tercera Edad</label>
					<input type="text" name="tedad" required class="form-control" placeholder="Ingrese cantidad de personas de tercera edad">
				</div>
				<div class="form-group col-sm-2">
					<label>Niños</label>
					<input type="text" name="ninos" required class="form-control" placeholder="Ingrese cantidad de niños">
				</div>
				<div class="form-group col-sm-6">
					<label>Salario</label>
					<input type="text" name="salario" required class="form-control" placeholder="Ingrese salario">
				</div>
				<div class="form-group col-sm-6">
					<label>Dias de descanso</label>
					<input type="text" name="ddescanso" required class="form-control" placeholder="Ingrese dias de descanso">
				</div>
				<div class="form-group col-sm-12 text-center">
					<label>
						Interna <input type="radio" name="ttrabajo" value="Interna">
					</label>
					<label>
						Externa <input type="radio" name="ttrabajo" value="Externa">
					</label>
					<label>
						Por dias <input type="radio" name="ttrabajo" value="Por dias">
					</label>
				</div>
				<div class="form-group col-sm-12">
					<label>Gestion</label>
					<textarea class="form-control" name="gestion" required placeholder="Ingrese su gestion"></textarea>
				</div>
				<div class="form-group col-sm-12">
					<h4 class="text-center titulosilver">Record</h4>
					<textarea class="form-control" name="historial" rows="15" required readonly disabled></textarea>
				</div>
				<div class="form-group col-sm-6">
					<label>Subir Imagen</label>
					<input type="file" name="imagen">
					<p class="text-justify">La imagen debe poseer las siguientes dimensiones: 150px de Ancho por 150px de Alto</p>
				</div>
				<div class="form-group col-sm-6 text-right">
					<a href="{{ route('home') }}" class="btn btn-personal">Inicio</a>
					<input type="submit" name="registrar" value="Guardar" class="btn btn-personal">
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection